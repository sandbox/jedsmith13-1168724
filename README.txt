This module changes the Cost field for a ubercart attribute to Suggested 
Retail. In the process it also renames the List Price field for products
to Suggested Retail and removes the Cost field for base products. It 
will also reformat the attribute text so that after the attribute it 
displays the Suggested Retail, Price, and amount saved.

When installed it will created a locale_custom_strings_en variable which
should add to any created by the String Overrides module if any exist.
